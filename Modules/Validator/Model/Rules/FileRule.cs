using System;
using Triton.Controller.Request;
using Triton.Utilities;

namespace Triton.Validator.Model.Rules {

#region History

// History:
//   2/4/14	- SD -	Added check for existence of a value in the Files collection before trying to
//					get the file identified by the Field property. Was throwing exception
//					if multiple possible file fields/rules existed and not all had a file
//					included.
//					Also consolidated adding of error to error collection.

#endregion

/// <summary>
/// <b>FileRule</b> is a leaf <b>IValidationRule</b> that validates
/// a file by size or type.
/// </summary>
///	<author>Scott Dyke</author>
public class FileRule : BaseLeafRule
{
	/// <summary>
	/// The file size limitation.
	/// </summary>
	private long? fileSize = null;


	/// <summary>
	/// Gets or sets the file length limitation.
	/// </summary>
	public string FileSize
	{
		get { 
			return fileSize.HasValue ? fileSize.ToString() : null; 
		}
		set { 
			fileSize = long.Parse(value); 
		}
	}


	/// <summary>
	/// Gets or sets the acceptable file types.
	/// </summary>
	public string FileType { 
		get; 
		set; 
	}


	/// <summary>
	/// Evaluates the rule for the given request.  The <b>RegexRule</b> applies its
	/// regular expression pattern to its specified field in the given request.
	/// </summary>
	/// <param name="request">The <b>MvcRequest</b> to evaluate the rule for.</param>
	/// <returns>A <b>ValidationResult</b> indicating the result of evaluating the
	///			rule and any validation errors that occurred.</returns>
	public override ValidationResult Evaluate(
		MvcRequest request)
	{
		ValidationResult result = new ValidationResult { Passed = true };

		if (!request.Files.ContainsKey(Field)) {
			result.Passed = false;
		} else {
					//  get the uploaded file reference
			MvcPostedFile theFile = request.Files[Field];

			if (FileType != null) {
				result.Passed = false;
						//  get the list of acceptable types
				string[] types = FileType.Split(',');
						//  get the extension of the file
				string ext = GetExtension(theFile.Name).ToLower();

						//  see if the file extension is in the acceptable list
				foreach (string t in types) {
					if (ext == t.ToLower()) {
						result.Passed = true;
						break;
					}
				}
			}

					//  if a file size limit is specified, check the size
// TODO: should we check result.Passed and not do this if we've already failed?? 
			if (fileSize.HasValue) {
				if (theFile.Length > fileSize) {
					result.Passed = false;
				}
			}
		}

				//  if the type check failed and an error ID was specified, add the error
		if (!result.Passed && (ErrorId > 0)) {
			result.AddError(Field, ErrorId);
		}

		return result;
	}


	/// <summary>
	/// Returns the file extension of the given file.
	/// </summary>
	/// <param name="filePath">The file name to get the extension for.</param>
	/// <returns>The file extension of the given file.</returns>
	private string GetExtension(
		string filePath)
	{
		string retVal = "";
		int pos = Math.Max(filePath.LastIndexOf('\\'), 0);

		pos = filePath.LastIndexOf('.', filePath.Length - 1, filePath.Length - pos);

		if (pos >= 0) {
			retVal = filePath.Substring(pos + 1);
		}

		return retVal;
	}
}
}