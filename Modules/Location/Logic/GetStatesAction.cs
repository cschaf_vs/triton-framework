﻿using System;
using System.Collections.Generic;
using Common.Logging;
using Triton.Controller;
using Triton.Controller.Action;
using Triton.Controller.Request;
using Triton.Location.Model;
using Triton.Location.Model.Dao;
using Triton.Model;
using Triton.Model.Dao;
using Triton.Logic.Support;

namespace Triton.Location.Logic {

#region History

// History:
// 08/13/2009	GV	Changed the inherited class to be Action after the rename from BizAction
//					and change the DaoFactory.GetDao method name
// 09/09/2009	GV	Changed the Action interface to inherit from IAction
//   3/6/2014	SD	Changed to use new FindState DAO method so as not to require IsTerritory
//					to be either true or false.

#endregion

public class GetStatesAction : IAction
{
	private const string ADDRESS_STATES = "address_states";


	public string StateItemNameOut
	{
		get;
		set;
	}
	/// <summary>
	/// Depricated. Use CountryItemNameOut instead.
	/// </summary>
	public string RequestItemName
	{
		get {
			return StateItemNameOut;
		}
		set {
			StateItemNameOut = value;
		}
	}


	#region IAction Members

	public string Execute(
		TransitionContext context)
	{
		MvcRequest request = context.Request;
		string retEvent = Events.Error;

		try {
			IStateDao dao = DaoFactory.GetDao<IStateDao>();

			StateFilter filter = dao.GetFilter();
					//  for now this just returns all the states
			// TODO: filter.Fill

			SearchResult<State> results = dao.FindState(filter);

			retEvent = EventUtilities.GetSearchResultEventName(results.Items.Length);

			request.Items[(StateItemNameOut ?? ADDRESS_STATES)] = results;
		} catch (Exception ex) {
			LogManager.GetLogger(typeof (GetStatesAction)).Error(
					errorMessage => errorMessage("GetStatesAction: Execute", ex));
		}

		return retEvent;
	}

	#endregion


	#region Nested type: Events

	/// <summary>
	/// Event names to return to the controller.
	/// </summary>
	public class Events
	{
		public static string Zero
		{
			get { return EventNames.ZERO; }
		}

		public static string One
		{
			get { return EventNames.ONE; }
		}

		public static string Multiple
		{
			get { return EventNames.MULTIPLE; }
		}

		public static string Error
		{
			get { return EventNames.ERROR; }
		}
	}

	#endregion
}
}