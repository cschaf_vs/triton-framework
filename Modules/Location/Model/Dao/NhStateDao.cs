﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Triton.Model;
using Triton.Model.Dao;
using Triton.NHibernate.Model.Dao;
using NHibernate;
using NHibernate.Criterion;

namespace Triton.Location.Model.Dao {

#region History

// History:
//    3/6/14 - SD - Added GetFilter and FindState.

#endregion

public class NhStateDao : NHibernateBaseDao<State>, IStateDao
{

	#region IStateDao Members

	public StateFilter GetFilter()
	{
		return new StateFilter();
	}


	public State Get(
		int id)
	{
		return base.Get(id);
	}


	public SearchResult<State> FindState(
		StateFilter filter)
	{
		SearchResult<State> results = null;

		try {
			StringBuilder where = new StringBuilder();
			ICriteria crit = this.Session.CreateCriteria(typeof(State), "st");

			if (filter.IsTerritory.HasValue) {
				crit.Add(Expression.Eq("st.IsTerritory", filter.IsTerritory.Value));
			}

			if (filter.Id.HasValue) {
				crit.Add(Expression.Eq("st.Id", filter.Id.Value));
			}

			if (!string.IsNullOrEmpty(filter.Code)) {
				crit.Add(Expression.Eq("st.Code", filter.Code));
			}

			IList<State> nhResults = crit.List<State>();

			results = new SearchResult<State>(nhResults.ToArray(), filter);

		} catch (Exception e) {
			//Common.Logging.LogManager.GetLogger(typeof(IStateDao)).Error(
			//		errorMessage => errorMessage(e.Message));
			throw;
		}

		return results;
	}

	#endregion
}
}