﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Triton.Location.Model.Dao;
using Triton.Model;
using Triton.Model.Dao;

namespace Triton.Location.Model {

#region History

// History:
//    3/6/14 - SD - Fixed LoadStates to actually load all states. Was using example query where IsTerritory
//					had to be either true or false making it impossible to load all states.

#endregion

/// <summary>
/// Singleton for storing location states for use in the deserialize address
/// </summary>
public class States : SingletonBase<States, State, string>
{
	private static States instance = null;

	private static object syncLock = new object();

	private List<State> states;


	public static States Instance
	{
		get {
			if (instance == null) {
				lock (syncLock) {
					if (instance == null) {
						instance = new States();
					}
				}
			}

			return instance;
		}
	}


	public States()
	{
		LoadStates();
	}


	/// <summary>
	/// Indexer to get a State by ID
	/// </summary>
	/// <param name="id">The ID of the state to get</param>
	/// <returns>The State with the given ID</returns>
	public State this[int id]
	{
		get { return states.FirstOrDefault(s => s.Id.Value == id); }
	}


	/// <summary>
	/// Loads the states from the database and populates singleton
	/// </summary>
	private void LoadStates()
	{
		IStateDao dao = DaoFactory.GetDao<IStateDao>();

		//List<State> loadResults = new List<State>(dao.Get(new State()));
		StateFilter filter = dao.GetFilter();
		SearchResult<State> results = dao.FindState(filter);

		if ((results != null) && (results.Items != null) && (results.Items.Length > 0)) {
			states = results.Items.ToList();
		}
	}
}
}
