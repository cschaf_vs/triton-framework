﻿namespace Triton.Location.Model {

#region History

// History:
//    3/6/14 - SD - Added CountryCode.

#endregion

public class State
{
	public virtual string Code { get; set; }

	public virtual string ShortName { get; set; }

	public virtual string LongName { get; set; }

	public virtual int? Id { get; set; }

	public virtual string CountryCode { get; set; }

	public virtual bool IsTerritory { get; set; }

	public virtual int? Version { get; set; }

	public virtual Region Region { get; set; }
}
}
